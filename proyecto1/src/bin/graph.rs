#![allow(unused)]
extern crate proyecto1;
extern crate diesel;

use self::proyecto1::*;
use self::models::*;
use std::collections::HashMap;
use assert_approx_eq::assert_approx_eq;

fn main() {
}

/*
 * Represents the graph by creating the adjacency matrix
 * 
 * @param relations => A vector of type Relation 
 * @param n => The number of cities 
 * @return A Graph
 * 
 * */
pub fn adjacency_matrix(relations: Vec<Relation>, cities: Vec<City>) -> Graph {
    let mut graph = Graph::default();
    let n = cities.len();
    let mut city_1: usize;
    let mut city_2: usize;
    graph.adjacency_m = vec![vec![0.0_f64; n+1]; n+1];

    for relation in relations{
        city_1 = relation.id_city_1 as usize;
        city_2 = relation.id_city_2 as usize;
        graph.adjacency_m[city_1][city_2] = relation.distance;
    }

    //Creating a hash for the cities
    for city in cities {
        graph.cities.insert(city.id, city);
    }

    return graph;
}
