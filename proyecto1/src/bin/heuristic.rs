#![allow(unused)]
extern crate proyecto1;
extern crate diesel;
extern crate ord_subset;

use self::proyecto1::*;
use self::models::*;
use std::collections::HashMap;
use assert_approx_eq::assert_approx_eq;

#[path = "solution.rs"] mod solution;
//import for unit tests
#[path = "data.rs"] mod data;
#[path = "graph.rs"] mod graph;

fn main (){
}

/*
 * Calculates batchs and returns the average of accepted solutions and
 * last solution accepted.
 * 
 * @param instance => A Instace struct
 * @param graph => A Graph structu
 * @param T => A f64 numbre
 * @return A tuple 
 * 
 */
pub fn acceptance_thresholds(instance_init: &mut Solution, graph: &Graph, temp: f64,
    rng: &mut rand_pcg::Lcg64Xsh32) -> Solution{

    let epsilon = 0.0001;
    let phi = 0.95;
    let mut p = 0.0;
    let mut q: f64;
    let mut tuple: (f64, Solution);
    let mut t = temp;
    let aux = define_properties(&instance_init.s, &graph);
    let normalizer = aux.0;
    let maximum = aux.1;
    solution::define_cost(instance_init, &graph, normalizer, maximum);
    let mut instance_aux = instance_init.clone();
    let mut best_solution = instance_init.clone();

    while (t > epsilon) {
        q = f64::INFINITY;
        while p < q{
            q = p;
            tuple = calculate_batch(&instance_aux, &graph, t, normalizer, maximum, rng);
            p = tuple.0;
            instance_aux = tuple.1;
            if(instance_aux.cost_f <= best_solution.cost_f){
                best_solution = instance_aux.clone();
            }
        }
        t *= phi;
    }

    return best_solution;
}

/*
 * Calculates batchs and returns the average of accepted solutions and
 * last solution accepted.
 */
fn calculate_batch(instance_init: &Solution, graph: &Graph, t: f64, normalizer: f64,
    maximum: f64, rng: &mut rand_pcg::Lcg64Xsh32) -> (f64, Solution){
    
    let mut c = 0;
    let mut r = 0.0;
    let mut h = 1;
    let l = (graph.cities.len()-1^2)/2;
    let halt_attempts = l*10;
    let mut new_solution: Solution;
    let mut solution = instance_init.clone();
    let mut average_solution = instance_init.clone();
    let mut a = 0;

    while (c < l && h < halt_attempts){
        new_solution = solution::get_neighbour(&solution.s, &graph, 
            normalizer, maximum, rng);
        if (new_solution.cost_f < solution.cost_f + t) {
            //data for the behaviour graph
            //println!("{}", new_solution.cost_f);
            solution = new_solution.clone();
            c += 1;
            r += new_solution.cost_f;
        }
        h += 1;
        average_solution = new_solution.clone();
    }

    return (r/(l as f64), solution);
}

/*
 * Calculates and returns the normalizer and maximum of the
 * original instance
 */
pub fn define_properties (s: &Vec<i32>, graph: &Graph) -> (f64, f64){
    let mut i = 0;
    let mut j = 0;
    let n = s.len();
    let mut id_1: usize;
    let mut id_2: usize;
    let mut l = vec![0.0_f64; 0];
    let mut maximum;
    let mut normalizer = 0.0;

    //create the L list
    while (i < n){
        id_1 = s[i] as usize;
        while (j < n){
            id_2 = s[j] as usize;
            if (graph.adjacency_m[id_1][id_2] != 0.0){
                l.push(graph.adjacency_m[id_1][id_2]);
            }
            j += 1;
        }
        j = 0;
        i += 1;
    }
    l.sort_by(|a, b| a.partial_cmp(b).unwrap());
    l.reverse();
    //define the right list
    if (l.len() >= n-1){
        l = Vec::from(&l[0..n-1]);
    }
    maximum = l[0];
    //sum all the distances
    for d in l{
        normalizer += d;
    }

    return (normalizer, maximum);
}


/*** Unit Tests ***/
#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_properties() {
        let mut instance_40 = Solution::default();
        instance_40.s = vec![1,2,3,4,5,6,7,163,164,165,168,172,327,329,331,332,333,489,490,491,
            492,493,496,653,654,656,657,661,815,816,817,820,823,871,978,979,980,981,982,984];
        let db_connection = data::Db::default();
        let aux = db_connection.get_data();
        let cities = aux.0;
        let relations = aux.1;    
        let mut g = graph::adjacency_matrix(relations, cities);
        //calculate properties
        let mut aux = define_properties(&instance_40.s, &g);
        let mut normalizer = aux.0;
        let mut maximum = aux.1;
        //test
        assert_approx_eq!(normalizer, 180836110.430000007, 2f64);
        assert_approx_eq!(maximum, 4947749.060000000, 2f64);

        //another instance
        let mut instance_150 = Solution::default();
        instance_150.s = vec![1,2,3,4,5,6,7,8,9,11,12,14,16,17,19,20,22,23,25,26,27,74,75,151,
            163,164,165,166,167,168,169,171,172,173,174,176,179,181,182,183,184,185,186,187,
            297,326,327,328,329,330,331,332,333,334,336,339,340,343,344,345,346,347,349,350,
            351,352,353,444,483,489,490,491,492,493,494,495,496,499,500,501,502,504,505,507,
            508,509,510,511,512,520,652,653,654,655,656,657,658,660,661,662,663,665,666,667,
            668,670,671,673,674,675,676,678,815,816,817,818,819,820,821,822,823,825,826,828,
            829,832,837,839,840,871,978,979,980,981,982,984,985,986,988,990,991,995,999,1001,
            1003,1004,1037,1038,1073,1075];
        //calculate properties
        aux = define_properties(&instance_150.s, &g);
        normalizer = aux.0;
        maximum = aux.1;
        //test
        assert_approx_eq!(normalizer, 723059620.720000267, 2f64);
        assert_approx_eq!(maximum, 4978506.480000000, 2f64);
    }
}