#![allow(unused)]
extern crate proyecto1;
extern crate diesel;
extern crate ord_subset;

use self::proyecto1::*;
use self::models::*;
use assert_approx_eq::assert_approx_eq;
use rand::{Rng, SeedableRng, rngs::StdRng};
use rand::prelude::*;
use rand_pcg::Pcg64;
use rand_seeder::{Seeder, SipHasher};

#[path = "graph.rs"] mod graph;
//import for unit tests
#[path = "data.rs"] mod data;


fn main(){
}

/*
 * Creates and returns an solution struct
 * @return solution
 * */
pub fn create_solution (s: Vec<i32>) -> Solution{
    let mut solution = Solution::default();
    solution.s = s;

    return solution;
}

/*
 * Creates and returns an solution struct, that is a 
 * neighbour of the recived solution
 * @param => A solution struct 
 * @return A solution
 * */
 pub fn get_neighbour (s: &Vec<i32>, graph: &Graph, normalizer: f64, maximum: f64,
    rng: &mut rand_pcg::Lcg64Xsh32) -> Solution{

    let mut solution_neighbour = Solution::default();
    solution_neighbour.s = s.to_vec();
    //random permutation of s    
    let mut id_1 = rng.gen_range(0,40);
    let mut id_2 = rng.gen_range(0,40);
    while(id_1 == id_2){
        id_2 = rng.gen_range(0,40);
    }
    let mut temp = solution_neighbour.s[id_1];
    solution_neighbour.s[id_1] = solution_neighbour.s[id_2];
    solution_neighbour.s[id_2] = temp;

    define_cost(&mut solution_neighbour, graph, normalizer, maximum);    

    return solution_neighbour;
}

/*
 * Evaluates the cost function for a given solution. 
 */
pub fn define_cost (solution: &mut Solution, graph: &Graph, normalizer: f64,
    maximum: f64) {

    let mut i = 1;
    let n = solution.s.len();
    let mut row: i32; 
    let mut col: i32;
    let mut new_cost = 0f64;
    //calculates the cost function
    while(i < n){
        row = solution.s[i-1];
        col = solution.s[i];
        //not in original graph
        if graph.adjacency_m[row as usize][col as usize] == 0f64 {
            let city_1 = graph.cities.get(&row).unwrap();
            let city_2 = graph.cities.get(&col).unwrap();
            new_cost += natural_distance(city_1, city_2) * maximum;
        } else{ //in original graph
            new_cost += graph.adjacency_m[row as usize][col as usize];
        }
        i += 1;
    }
    //normalize cost
    solution.cost_f = new_cost/normalizer;
}

/*
 * Calculates the natural distance between two cities
 * @param city_1, city_2 => Two tuples of type f64
 * @return distance => The distance between the cities
 * 
 * */
pub fn natural_distance(city_1: &City, city_2: &City) -> f64{
    let r = 6373000f64;
    let start_lat = city_1.lat.to_radians();
    let start_long = city_1.long.to_radians();
    let end_lat = city_2.lat.to_radians();
    let end_long = city_2.long.to_radians();
    
    let delta_latitude = (end_lat - start_lat) / 2.0;
    let delta_longitude = (end_long - start_long) / 2.0; 
    let a = delta_latitude.sin().powi(2) + 
                start_lat.cos()*end_lat.cos()*delta_longitude.sin().powi(2);
    let c = 2.0*(a.sqrt().atan2((1.0-a).sqrt()));

    //round distance with two decimals
    let x = (r*c * 100.0).round() / 100.0;
    return x;
}


/*** Unit Tests ***/
#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_cost_40_150() {
        let mut solution_40 = Solution::default();
        solution_40.s = vec![1,2,3,4,5,6,7,163,164,165,168,172,327,329,331,332,333,489,490,491,
            492,493,496,653,654,656,657,661,815,816,817,820,823,871,978,979,980,981,982,984];
        let db_connection = data::Db::default();
        let aux = db_connection.get_data();
        let cities = aux.0;
        let relations = aux.1;
        let mut g = graph::adjacency_matrix(relations, cities);
        //evaluate cost function
        let mut normalizer = 180836110.430000007;
        let mut maximum = 4947749.060000000;
        define_cost(&mut solution_40, &g, normalizer, maximum);
        //test
        assert_approx_eq!(solution_40.cost_f, 3305585.454990047, 2f64);

        //another solution
        let mut solution_150 = Solution::default();
        solution_150.s = vec![1,2,3,4,5,6,7,8,9,11,12,14,16,17,19,20,22,23,25,26,27,74,75,151,
            163,164,165,166,167,168,169,171,172,173,174,176,179,181,182,183,184,185,186,187,
            297,326,327,328,329,330,331,332,333,334,336,339,340,343,344,345,346,347,349,350,
            351,352,353,444,483,489,490,491,492,493,494,495,496,499,500,501,502,504,505,507,
            508,509,510,511,512,520,652,653,654,655,656,657,658,660,661,662,663,665,666,667,
            668,670,671,673,674,675,676,678,815,816,817,818,819,820,821,822,823,825,826,828,
            829,832,837,839,840,871,978,979,980,981,982,984,985,986,988,990,991,995,999,1001,
            1003,1004,1037,1038,1073,1075];
        //evaluate cost function
        normalizer = 723059620.720000267;
        maximum = 4978506.480000000;
        define_cost(&mut solution_150, &g, normalizer, maximum);
        //test
        assert_approx_eq!(solution_150.cost_f, 6152051.625245280, 2f64);
    }

    #[test]
    //verify all distances in DB (original graph) with the system's function
    fn test_natural_d() {
        //db connection
        let db_connection = data::Db::default();
        let aux = db_connection.get_data();
        let cities = aux.0;
        let relations = aux.1;
        //original graph
        let mut g = graph::adjacency_matrix(relations, cities);
        let mut i = 0;
        let mut j = 0;
        let n = g.adjacency_m.len();
        let mut system_value: f64;
        let mut db_value: f64;
        let mut city_1 = &City::default();
        let mut city_2 = &City::default();
        let mut id_1: i32;
        let mut id_2: i32;
        //stablish value of all comparations
        let mut valid = true;

        while (i < n){
            j = 0;
            while (j < n){
                db_value = g.adjacency_m[i][j];
                if db_value != 0f64{
                    id_1 = i as i32;
                    id_2 = j as i32;
                    city_1 = g.cities.get(&id_1).unwrap();
                    city_2 = g.cities.get(&id_2).unwrap();
                    system_value = natural_distance(city_1, city_2);
                    if system_value != db_value{
                        valid = false;
                    }
                }
                j += 1;
            }
            i += 1;
        }

        assert_eq!(valid, true);
    }
}
