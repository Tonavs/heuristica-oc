extern crate proyecto1;
extern crate diesel;

mod data;
mod graph;
mod solution;
mod heuristic;

use rand::SeedableRng;
use rand::seq::SliceRandom;
use std::env;

pub fn main() {
    let args: Vec<String> = env::args().collect();
    let seed = args[1].parse::<u64>().unwrap();
    //random generator 
    let mut rng = rand_pcg::Pcg32::seed_from_u64(seed);
    //Data from DB
    let db_connection = data::Db::default();
    let aux = db_connection.get_data();
    let cities = aux.0;
    let relations = aux.1;

    //fixed, for now, instances
    let mut s_40 = vec![1,2,3,4,5,6,7,163,164,165,168,172,327,329,331,332,333,489,490,491,492,
        493,496,653,654,656,657,661,815,816,817,820,823,871,978,979,980,981,982,984];
    let _s_150 = vec![1,2,3,4,5,6,7,8,9,11,12,14,16,17,19,20,22,23,25,26,27,74,75,151,
        163,164,165,166,167,168,169,171,172,173,174,176,179,181,182,183,184,185,186,187,
        297,326,327,328,329,330,331,332,333,334,336,339,340,343,344,345,346,347,349,350,
        351,352,353,444,483,489,490,491,492,493,494,495,496,499,500,501,502,504,505,507,
        508,509,510,511,512,520,652,653,654,655,656,657,658,660,661,662,663,665,666,667,
        668,670,671,673,674,675,676,678,815,816,817,818,819,820,821,822,823,825,826,828,
        829,832,837,839,840,871,978,979,980,981,982,984,985,986,988,990,991,995,999,1001,
        1003,1004,1037,1038,1073,1075];

    //create the original graph
    let g = graph::adjacency_matrix(relations, cities);

    //initial rand solution
    s_40.shuffle(&mut rng);
    let mut solution_init = solution::create_solution(s_40);
    //temperature -> fixed for now
    let temp = 120000.0;
    let best_solution = heuristic::acceptance_thresholds(&mut solution_init, &g, temp, &mut rng);
    println!("Solution: {:?}", best_solution.s);
    println!("Cost: {:?}", best_solution.cost_f);

}
