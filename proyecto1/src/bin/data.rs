#![allow(unused)]
extern crate proyecto1;
extern crate diesel;

use self::proyecto1::*;
use self::models::*;
use self::diesel::prelude::*;

pub struct Db{
    data_connect : diesel::SqliteConnection,
}
impl Default for Db {
    fn default () -> Db {
        Db{data_connect: establish_connection()}
    }
}
impl Db{
    /* 
     * Gets the data from the DB 
     * 
     * @return A tuple
     *
     * */
    pub fn get_data(self) -> (Vec<City>, Vec<Relation>) {
        use proyecto1::schema::cities::dsl::*;
        use proyecto1::schema::relations::dsl::*;

        let data_cities = cities.load::<City>(&self.data_connect)
                            .expect("Error loading cities");

        let data_relations = relations.load::<Relation>(&self.data_connect)
                                .expect("Error loading cities");

        return (data_cities, data_relations);
    }
}

fn main(){
}