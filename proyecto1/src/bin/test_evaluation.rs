#![allow(unused)]
extern crate proyecto1;
extern crate diesel;

use self::proyecto1::*;
use self::models::*;
use rand::prelude::*;

#[path = "graph.rs"] mod graph;
#[path = "data.rs"] mod data;
#[path = "heuristic.rs"] mod heuristic;
#[path = "instance.rs"] mod instance;

//main for the given tests for paths evaluations
pub fn main (){
    //Data from DB
    let db_connection = data::Db::default();
    let aux = db_connection.get_data();
    let cities = aux.0;
    let relations = aux.1;
    //original graph
    let g = graph::adjacency_matrix(relations.clone(), cities.clone());
    //given random paths
    let path_1 = vec![1,163,164,327,489,491,722,815,978,980];
    let path_2 = vec![1,2,163,409,489,490,653,815,816,978];
    let path_3 = vec![1,2,163,419,489,490,653,815,816,978];
    let path_4 = vec![1,2,163,489,490,653,683,815,816,978];
    let path_5 = vec![1,2,163,489,490,653,815,816,912,978];
    //system solutions
    let mut system_values = vec![0f64; 0];
    system_values.push(evaluate_solution(path_1, &g, &cities, &relations));
    system_values.push(evaluate_solution(path_2, &g, &cities, &relations));
    system_values.push(evaluate_solution(path_3, &g, &cities, &relations));
    system_values.push(evaluate_solution(path_4, &g, &cities, &relations));
    system_values.push(evaluate_solution(path_5, &g, &cities, &relations));
    //correct evaluations
    let knk_value_0 = 3741782.581560322;
    let knk_value_1 = 2622238.354438104;
    let knk_value_2 = 2528965.918692719;
    let knk_value_3 = 1715870.207070505;
    let knk_value_4 = 2386926.526032270;
    println!("Casi iguales...");
    println!("{}", system_values[0] - knk_value_0);
    println!("{}", system_values[1] - knk_value_1);
    println!("{}", system_values[2] - knk_value_2);
    println!("{}", system_values[3] - knk_value_3);
    println!("{}", system_values[4] - knk_value_4);
}

//returns cost function
fn evaluate_solution (path: Vec<i32>, g: &Graph, cities: &Vec<City>,
    relations: &Vec<Relation>) -> f64{

    let temp = heuristic::define_properties(&path, &g);
    //get the correct normalizer and maximum
    let normalizer = temp.0;
    let maximum = temp.1;
    //create solution
    let mut solution = instance::create_instance(path);
    instance::define_cost(&mut solution, &g, normalizer, maximum);
    
    return solution.cost_f;
}
