use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Queryable, Clone)]
pub struct City {
    pub id : i32,
    pub name: String,
    pub country: String,
    pub population: i32,
    pub lat: f64,
    pub long: f64,
}
impl Default for City {
    fn default () -> City {
        City{id:0, name:String::new(), country:String::new(), population:0,
            lat:0.0_f64, long:0.0_f64}
    }
}

#[derive(Queryable, Copy, Clone)]
pub struct Relation {
    pub id_city_1 : i32,
    pub id_city_2 : i32,
    pub distance: f64,
}
impl PartialOrd for Relation {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.distance.partial_cmp(&other.distance)
    }
}
impl PartialEq for Relation {
    fn eq(&self, other: &Self) -> bool {
        self.distance == other.distance
    }
}
impl Default for Relation {
    fn default () -> Relation {
        Relation{id_city_1:0, id_city_2:0, distance:0.0_f64}
    }
}

#[derive(Clone)]
pub struct Graph {
    pub adjacency_m : Vec<std::vec::Vec<f64>>,
    pub cities : HashMap<i32, City>,
}
impl Default for Graph {
    fn default () -> Graph {
        Graph{adjacency_m:vec![vec![0.0; 0]; 0], cities:HashMap::new()}
    }
}

#[derive(Clone)]
pub struct Solution {
    pub s : Vec<i32>,
    pub cost_f : f64,
}
impl Default for Solution {
    fn default () -> Solution {
        Solution{s:vec![0; 0], cost_f: 0.0_f64}
    }
}