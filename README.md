# README

- Repositorio para proyectos del seminario: Heurísticas de Optimización Combinatoria. 
- Proyectos hechos en Rust (según).

### Comandos:

- Para ejecutar el proyecto: `cargo run --bin main <seed>` o `cargo run --release --bin main <seed>`
      - Siendo la semilla un valor numérico
- Para ejecutar las pruebas unitarias: `cargo test`